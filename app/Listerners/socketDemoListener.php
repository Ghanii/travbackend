<?php

namespace App\Listerners;

use App\Events\socketDemo;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class socketDemoListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  socketDemo  $event
     * @return void
     */
    public function handle(socketDemo $event)
    {
        //
    }
}
