<?php


namespace App\Services\Dossier;


interface DossierInterface
{
    public function getAllDossier();
    public function getDossier($dossier_id);
    public function getClinet($dossier_id);
    public function getTraveaux($dossier_id);

}

