<?php


namespace App\Services\Dossier;


use App\Exceptions\DossierExption;
use App\Models\Dossier;
use mysql_xdevapi\Exception;

class DossierService implements DossierInterface
{
    protected  $Dossier ;
    public function __construct(Dossier $dossier)
    {
        $this->Dossier = $dossier ;
    }

    public function getAllDossier()
    {
        try {
            return $this->Dossier::all();
        }catch (\Exception $e){
            // throw new DossierExption(DossierExption::GET_ALL_DOSSIER_EXCEPTION);
            return  "data base issues , please try later" ;
        }

    }

    public function getDossier($dossier_id)
    {
        try {
            return $this->Dossier::findOrFail($dossier_id) ;
        }catch (\Exception $e){
            return  response()->json([
                'message' => "data base issues , please try later"
            ], 500) ;
        }

    }

    public function getClinet($dossier_id)
    {
        try {
            return $this->getDossier($dossier_id)->client()->get();
        }catch (\Exception $e){
            return  "data base issues , please try later" ;
        }

    }

    public function getTraveaux($dossier_id)
    {
        try {
            $dossier = $this->getDossier($dossier_id);
            return !empty($dossier)? $dossier->traveaux()->get():null;
        }catch (\Exception $e){
            return  "data base issues , please try later" ;
        }

    }


}
