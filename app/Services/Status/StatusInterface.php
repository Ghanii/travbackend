<?php


namespace App\Services\Status;


interface StatusInterface
{
    public function getAllStatus();
    public function getStatusChildren($parent_id) ;
    public function getStatusParent($child_id) ;
    public function getStatusWithSameParent($pid);


}
