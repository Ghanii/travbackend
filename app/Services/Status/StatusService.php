<?php


namespace App\Services\Status;


use App\Models\Status;

class StatusService implements StatusInterface
{
    protected $Status;
    public function __construct(Status $status)
    {
        $this->Status = $status ;
    }

    public function getAllStatus() {
        try {
            //return $this->Status::all()->where('pid' , '!=' ,null)->groupBy('pid');
            return $this->Status::where('pid',"!=" , null)->get() ;
        }catch (\Exception $e){
            return "data base problems , try later";
        }

    }

    public function getStatusWithSameParent($key) {
        try {
             $sts = $this->Status::where('key' , $key)->get() ;
            return $this->Status::where('pid' , $sts[0]->pid)->get();

        }catch (\Exception $e){
            return "data base problems , try later";
        }
    }

    public function getStatusChildren($parent_id)
    {
        try {
            return $this->Status::where('pid' , $parent_id)->get();
        }catch (\Exception $e){
            return "data base problems , try later";
        }

    }

    public function getStatusParent($child_id)
    {
        try {
            $parent = $this->Status::where('key',$child_id)->get()->first();
            $pid = $parent->pid  ;
            if($pid !=null){
                return $this->Status::where('key',$pid)->get();
            }else{
                return $parent ;
            }

        }catch (\Exception $e){
            return "data base problems , try later";
        }


    }
}
