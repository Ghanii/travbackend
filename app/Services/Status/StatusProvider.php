<?php

namespace App\Services\Status;

use Illuminate\Support\ServiceProvider;

class StatusProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(StatusInterface::class , StatusService::class);
    }
}
