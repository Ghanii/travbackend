<?php


namespace App\Services\agent;


use App\Models\Agent;
use App\Models\Ouvrier;
use App\Services\Dossier\DossierInterface;
use App\Services\Status\StatusInterface;
use App\Services\Traveaux\TraveauxInterface;
use App\Services\Traveaux\TraveauxService;

class AgentService implements AgentInterface
{
    protected $agent ;

    /* @var TraveauxService $traveaux */
    protected $traveaux ;

    protected $dossier ;
    protected $status ;
    protected $ouvrier ;

    public function __construct(
        Agent $agent,
        TraveauxInterface $traveaux ,
        DossierInterface $dossier,
        StatusInterface $status,
        Ouvrier $ouvrier
    )
    {
        $this->agent = $agent;
        $this->traveaux = $traveaux;
        $this->dossier = $dossier;
        $this->status = $status;
        $this->ouvrier = $ouvrier;
    }

    public function getDossiersAgent($id_agent)
    {
       return $this->agent::find($id_agent)->dossiers()->get('id');


    }

    public function getINfo($id_agent , $id_doss)
    {
        $dossier = $this->getDossiersAgent($id_agent);
        $trv = null;
        foreach ($dossier as $doss) {
           if($doss->id == $id_doss) {
                $trv = $this->dossier->getTraveaux($doss->id) ;
                //return $this->getStatusDossier($trv);
               $statusDossier = $this->getStatusDossier($trv);
               $traveaxDossier = $this->traveaux->getTraveauxOfStatus($statusDossier['child']->key);
               $statusTraveauDossier = $this->status->getStatusChildren($statusDossier['parent']->key);
               return [
                   "allTraveaux" => $trv ,
                   "traveauxDossier" => $traveaxDossier,
                   "allStatus" => "stss",
                   "statusOfTraveaux" => $statusTraveauDossier ,
                   "statusDossier" => $statusDossier['child'],
                   "instalateur" => $this->traveaux->getTraveauxOuvrier($traveaxDossier[0]->id),
                   "ouvriers" => $this->ouvrier::all(),
               ];

            }

        }

    }


    public function statuDossier($id_dossier) {
        $trv = $this->dossier->getTraveaux($id_dossier) ;
        $statusDossier = $this->getStatusDossier($trv);
        return $statusDossier ;
    }


    //just to not crush the code
    private function getStatusDossier($traveaux)
    {
        $status = [];
        foreach ($traveaux as $indx=>$trav) {
            $sts = $this->traveaux->getTraveauxStatus($trav->id);
            $par = $this->status->getStatusParent($sts[0]->key);
            array_push($status , array("parent" => $par[0] , "child" => $sts[0])) ;

        }

        return $this->getStatusMin($status);

    }

    private  function  getStatusMin($status) {
        $min = $status[0] ;
        for( $i = 1 ; $i < count($status) ; $i++) {
            if($status[$i]['parent']->ordre < $min['parent']->ordre){
                $min = $status[$i];
            }
            elseif ($status[$i]['parent']->ordre == $min['parent']->ordre){
                //$min[] = $status[$i];
                if( $status[$i]['child']->ordre < $min['child']->ordre ) {
                    $min = $status[$i] ;
                }
            }

        }

        return $min ;
    }


    // new ones

    public function getInfoTraveaux($id_traveaux) {
        /*$status = null ;
        $allStatus = null;
        $ouvrier = null;*/
        try {
            $status = $this->traveaux->getTraveauxStatus($id_traveaux);
            $ouvrier = $this->traveaux->getTraveauxOuvrier($id_traveaux);
            $allStatus = $this->status->getAllStatus();
            $allOuvrier = $this->ouvrier::all();
            return response()->json([
                "status" => $status ,
                "ouvrier" => $ouvrier ,
                "allStatus" =>$allStatus ,
                "allOuvrier" => $allOuvrier
            ]);

        }catch (\Exception $e){
            return "bad req";
        }

    }


}
