<?php


namespace App\Services\agent;


interface AgentInterface
{
    /**
     * to get all dossier of that agent
     *
     * @param $id_agent
     * @return mixed
     */
    public function getDossiersAgent($id_agent);

    /**
     * to get info for the form in the backoffice front end
     *
     * @param int $id_agent
     * @param int $id_doss
     * @return mixed
     */
    public function getINfo($id_agent, $id_doss);

    public function getInfoTraveaux($id_traveaux);

    public  function statuDossier($id_dossier) ;





}
