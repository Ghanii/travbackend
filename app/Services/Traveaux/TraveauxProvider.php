<?php

namespace App\Services\Traveaux;

use Illuminate\Support\ServiceProvider;

class TraveauxProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(TraveauxInterface::class , TraveauxService::class);
    }
}
