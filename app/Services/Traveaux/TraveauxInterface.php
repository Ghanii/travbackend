<?php


namespace App\Services\Traveaux;


use App\Models\Traveaux;

interface TraveauxInterface
{
    /**
     * get travaux that belong to the same folder
     *
     * @param int $dossier_id
     * @return Traveaux|null
     */
    public function getTraveauxWithSameDossier($dossier_id);

    public function getTraveauxOuvrier($id);
    public function getTraveauxStatus($status_key);

    public function getTraveauxOfStatus($id_status);

    public function updateStatusOfTraveaux($id_traveaux , $status_key , $instal_id);
}
