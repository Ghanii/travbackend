<?php


namespace App\Services\Traveaux;


use App\Models\Status;
use App\Models\Traveaux;

class TraveauxService implements TraveauxInterface
{
    protected $traveaux ;
    public function __construct(Traveaux $traveaux)
    {
        $this->traveaux = $traveaux ;
    }

    public function getTraveauxWithSameDossier($dossier_id)
    {
        try {
            return $this->traveaux::where('dossier_id' , $dossier_id)->get() ;
        }catch (\Exception $e){
            return "data base problems , try later";
        }

    }

    public function getTraveauxOuvrier($id)
    {
        try {
            return $this->traveaux::findOrFail($id)->ouvrier()->get();
        }catch (\Exception $e){
            return "data base problems , try later";
        }
    }

    public function getTraveauxStatus($id)
    {
        try {
            return $this->traveaux::findOrFail($id)->status()->get() ;
        }catch (\Exception $e){
            return "data base problems , try later";
        }
    }

    public function getTraveauxOfStatus($id_status) {
        try {
            return $this->traveaux::where('statuse_key' , $id_status)->get();
        }catch (\Exception $e){
            return "data base problems , try later";
        }
    }

    public function updateStatusOfTraveaux($id_traveaux , $status_key , $instal_id){
        try {
            //return $id_traveaux.$status_key;
            $traveauxToUpdate = Traveaux::findOrFail($id_traveaux);

            //$statusToInactive = $traveauxToUpdate->status()->get()->first();
            //$statusToInactive->active = false ;
            //$statusToInactive->save();

            $traveauxToUpdate->statuse_key = $status_key;
            $traveauxToUpdate->ouvrier_id = $instal_id;
            $traveauxToUpdate->save();



            //$statusToActive = Status::findOrFail($status_key);
            //$statusToActive->active = true ;
            //$statusToActive->save();


            return response()->json(['status' => "updated successfully"]) ;
        }
        catch (\Exception $e){
            return "data base prob try later";
        }
    }
}
