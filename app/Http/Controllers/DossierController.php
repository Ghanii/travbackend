<?php

namespace App\Http\Controllers;

use App\Services\Dossier\DossierInterface;
use Illuminate\Http\Request;

class DossierController extends Controller
{
    protected $dossier ;
    public function __construct(DossierInterface $doss)
    {
        $this->dossier = $doss ;
    }

    public  function  test() {
        return $this->dossier->getAllDossier();
    }
    public function dossier($id) {
        return $this->dossier->getDossier($id) ;
    }
    public function client($id) {
        return $this->dossier->getClinet($id);
    }
    public function Traveaux($id) {
        return $this->dossier->getTraveaux($id);
    }
}
