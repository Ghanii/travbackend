<?php

namespace App\Http\Controllers;

use App\Services\Traveaux\TraveauxInterface;
use Illuminate\Http\Request;

class TraveauxController extends Controller
{
    protected $traveaux ;
    public function __construct(TraveauxInterface $traveaux)
    {
        $this->traveaux = $traveaux ;
    }

    public function test($id_doss) {
        return $this->traveaux->getTraveauxWithSameDossier($id_doss) ;
    }

    public function ouvrier($id) {
        return $this->traveaux->getTraveauxOuvrier($id);
    }
    public function status($id) {
        return $this->traveaux->getTraveauxStatus($id);
    }

    public function updateStatusOfTraveaux($id_traveaux , $statuse_key , $instal_id){
        return $this->traveaux->updateStatusOfTraveaux($id_traveaux , $statuse_key , $instal_id);
    }
}
