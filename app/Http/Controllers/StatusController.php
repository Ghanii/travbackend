<?php

namespace App\Http\Controllers;

use App\Services\Status\StatusInterface;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    protected $status ;
    public function __construct(StatusInterface $status)
    {
        $this->status = $status;
    }

    public function test() {
        return $this->status->getAllStatus();
    }

    public function child($id_p) {
        return $this->status->getStatusChildren($id_p);
    }

    public function parent($id_c) {
        return $this->status->getStatusParent($id_c);
    }

    public function statusSamePar($pid) {
        return $this->status->getStatusWithSameParent($pid);
    }
}
