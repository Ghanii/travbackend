<?php

namespace App\Http\Controllers;

use App\Services\agent\AgentInterface;
use Illuminate\Http\Request;

class AgentController extends Controller
{
    protected $agent ;
    public function __construct(AgentInterface $agent)
    {
        $this->agent = $agent ;
    }

    public function getDossiers($id_agent){
        return $this->agent->getDossiersAgent($id_agent);

    }

    public  function getStatusDossier($id_agent , $id_doss) {
        return $this->agent->getINfo($id_agent , $id_doss);
    }

    public function getTraveauxInfo($id_traveaux) {
        return $this->agent->getInfoTraveaux($id_traveaux);
    }

    public function statusDossier($id_dossier){
        return $this->agent->statuDossier( $id_dossier ) ;
    }
}
