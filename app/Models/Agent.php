<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    public function dossiers() {
        return $this->hasMany(Dossier::class);
    }
}
