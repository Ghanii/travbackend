<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dossier extends Model
{
    public function traveaux() {
        return $this->hasMany(Traveaux::class);
    }

    public function client() {
        return $this->belongsTo(Client::class);
    }
    public function agent() {
        return $this->belongsTo(Agent::class);
    }
}
