<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Traveaux extends Model
{
    public function dossier() {
        return $this->belongsTo(Dossier::class);
    }
    public function status() {
        return $this->hasOne(Status::class , 'key' , 'statuse_key');
    }

    public function ouvrier() {
        return $this->belongsTo(Ouvrier::class );
    }

}
