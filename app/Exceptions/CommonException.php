<?php

namespace App\Exceptions;

use Exception;
use Throwable;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class CommonException extends Exception
{
    private $code;

    public function __construct($code)
    {
        $this->code = $code;
        parent::__construct("", $code, null);
    }

    public function render() {
        $message = trans('exception_' . $this->code);

    }
}
