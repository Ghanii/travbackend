<?php

use Illuminate\Support\Facades\Route;
use  \App\Http\Controllers\DossierController;
use \App\Http\Controllers\StatusController;
use \App\Http\Controllers\TraveauxController;
use \App\Http\Controllers\AgentController;
use App\Events\socketDemo;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  // broadcast(new socketDemo('what up sockets'));
    //event(new socketDemo('what up'));
   return view('welcome');
});



Route::get('/dossier' , 'DossierController@test') ;
Route::get('/dossier/{id}' , 'DossierController@dossier') ;
Route::get('/dossier/{id}/client' , 'DossierController@client') ;
Route::get('/dossier/{id}/traveaux' , 'DossierController@Traveaux') ;

Route::get('/status' , 'StatusController@test') ;
Route::get('/status/{idp}/child' , 'StatusController@child') ;
Route::get('/status/{idc}/parent' , 'StatusController@parent') ;
Route::get('/status/{idc}/brothers' , 'StatusController@statusSamePar') ;


Route::get('/traveaux/{idd}' , 'TraveauxController@test') ;
Route::get('/traveaux/{id}/ouvrier' , 'TraveauxController@ouvrier') ;
Route::get('/traveaux/{id}/status' , 'TraveauxController@status') ;
Route::get('/traveaux/updateStatus/{id_traveaux}/{statuse_key}/{instal_id}' , 'TraveauxController@updateStatusOfTraveaux');

Route::get('/agent/{id}/dossiers' ,'AgentController@getDossiers');
Route::get('/agent/{id}/statusDossier/{iddoss}' ,'AgentController@getStatusDossier' );
Route::get('/agent/traveauxInfo/{id_trv}' ,'AgentController@getTraveauxInfo' );
