<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->id('key');
            $table->integer("pid")->nullable();
            $table->string("libelle");
            $table->string("description");
            $table->integer("ordre");
            $table->boolean("enabled");
            $table->boolean('active')->nullable();
            $table->boolean("show_for_client");
            $table->boolean("show_for_agent");
            $table->boolean("show_only_when_active") ;
            $table->string("show_for_clients");
            $table->string("components")->nullable() ;
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statuses');
    }
}
