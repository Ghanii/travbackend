<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTraveauxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traveauxes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('dossier_id');
            $table->foreign('dossier_id')->references('id')->on('dossiers');
            $table->unsignedBigInteger("ouvrier_id");
            $table->foreign("ouvrier_id")->references("id")->on("ouvriers") ;
            $table->unsignedBigInteger('statuse_key');
            $table->foreign('statuse_key')->references("key")->on("statuses") ;
            $table->date('date');
            $table->string('types_de_traveaux');
            $table->integer('surface');
            $table->string('type_de_chaufage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traveauxes');
    }
}
