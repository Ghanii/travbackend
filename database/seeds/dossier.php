<?php

use Illuminate\Database\Seeder;

class dossier extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1 ; $i <= 3 ; $i++) {
            $dossier = new \App\Models\Dossier();
            $dossier->client_id = $i ;
            $dossier->agent_id = $i ;
            $dossier->save() ;
        }
    }
}
