<?php

use Illuminate\Database\Seeder;

class traveaux extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $traveaux = [
           [   'date' => Carbon\Carbon::now() ,
               'types_de_traveaux' => "chaufage" ,
               'surface' => 50 ,
               'type_de_chaufage' => 'electricity'
           ] ,
            [   'date' => Carbon\Carbon::now() ,
                'types_de_traveaux' => "chaufage" ,
                'surface' => 100 ,
                'type_de_chaufage' => 'gaz'
            ] ,
            [   'date' => Carbon\Carbon::now() ,
                'types_de_traveaux' => "chaufage" ,
                'surface' => 500 ,
                'type_de_chaufage' => 'electricity'
            ] ,
            [   'date' => Carbon\Carbon::now() ,
                'types_de_traveaux' => "chaufage" ,
                'surface' => 100 ,
                'type_de_chaufage' => 'electricity'
            ] ,
            [   'date' => Carbon\Carbon::now() ,
                'types_de_traveaux' => "chaufage" ,
                'surface' => 300 ,
                'type_de_chaufage' => 'electricity'
            ] ,
            [   'date' => Carbon\Carbon::now() ,
                'types_de_traveaux' => "chaufage" ,
                'surface' => 400 ,
                'type_de_chaufage' => 'electricity'
            ] ,

        ];
        $i=1 ;

        foreach ($traveaux as $index => $travail) {
            $tr = new \App\Models\Traveaux();
            $tr->ouvrier_id = 1 ;
            if($i < 2) {
                $tr->dossier_id = 1 ;
            } elseif ($i < 4 && $i > 1) {
                $tr->dossier_id = 2 ;
            } else {
                $tr->dossier_id = 3 ;
            }
            $tr->statuse_key = $i ;
            $tr->date = $travail['date'] ;
            $tr->types_de_traveaux = $travail['types_de_traveaux'];
            $tr->surface = $travail['surface'];
            $tr->type_de_chaufage = $travail['type_de_chaufage'];
            $tr->save();
            $i++;
        }
    }
}
