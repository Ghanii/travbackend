<?php

use Illuminate\Database\Seeder;

class ouvrier extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ouvriers = [
            ['artisan' => 'john legend' , 'telephone' => '06152236'] ,
            ['artisan' => 'newman said' , 'telephone' => '06152236'] ,
            ['artisan' => 'omar legend' , 'telephone' => '06152236'] ,
            ['artisan' => 'zayn sami' , 'telephone' => '06152236'] ,
        ];
        foreach ($ouvriers as $ouvrier) {
            $ouv = new \App\Models\Ouvrier();
            $ouv->artisan = $ouvrier['artisan'] ;
            $ouv->telephone = $ouvrier['telephone'] ;
            $ouv->save() ;
        }
    }
}
