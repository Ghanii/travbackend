<?php

use Illuminate\Database\Seeder;

class status extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = [
            [   'pid' => null ,
                'libelle' => 'visite technique' ,
                'description' => 'je confirme ma visite technique',
                'ordre' => 1 ,
                'enabled' => true,
                'active'=> null ,
                'show_for_client' => true,
                'show_for_agent' => true ,
                'show_only_whene_active' => false ,
                'show_for_clients' => 'rehagi , lafarge',
                'components' => null
            ] ,
            [   'pid' => null ,
                'libelle' => "confirmation d'elegibilite ",
                'description' => 'je fais ma simulation en ligne',
                'ordre' => 2 ,
                'enabled' => true,
                'active'=> null ,
                'show_for_client' => true,
                'show_for_agent' => true ,
                'show_only_whene_active' => false ,
                'show_for_clients' => 'rehagi , lafarge',
                'components' => null
            ] ,
            [   'pid' => null ,
                'libelle' => 'visite normale' ,
                'description' => 'je confirme ma visite ',
                'ordre' => 3 ,
                'enabled' => true,
                'active'=> null ,
                'show_for_client' => true,
                'show_for_agent' => true ,
                'show_only_whene_active' => false ,
                'show_for_clients' => 'rehagi , lafarge',
                'components' => null
            ] ,
            [   'pid' => 1 ,
                'libelle' => 'rendez-vous ' ,
                'description' => 'je prend rendez-vous avec artisan',
                'ordre' => 1 ,
                'enabled' => true,
                'active'=> true ,
                'show_for_client' => true,
                'show_for_agent' => true ,
                'show_only_whene_active' => false ,
                'show_for_clients' => 'rehagi , lafarge',
                'components' => null
            ] ,
            [   'pid' => 1 ,
                'libelle' => 'visite technique' ,
                'description' => 'je pass ma visite technique',
                'ordre' => 2 ,
                'enabled' => true,
                'active'=> false ,
                'show_for_client' => true,
                'show_for_agent' => true ,
                'show_only_whene_active' => false ,
                'show_for_clients' => 'rehagi , lafarge',
                'components' => null
            ] ,
            [   'pid' => 1 ,
                'libelle' => 'valider devise' ,
                'description' => 'je valide',
                'ordre' => 3 ,
                'enabled' => true,
                'active'=> false ,
                'show_for_client' => true,
                'show_for_agent' => true ,
                'show_only_whene_active' => false ,
                'show_for_clients' => 'rehagi , lafarge',
                'components' => 'devise'
            ] ,
            [   'pid' => 2 ,
                'libelle' => 'simulation' ,
                'description' => 'je fais ma simulation',
                'ordre' => 1 ,
                'enabled' => true,
                'active'=> false ,
                'show_for_client' => true,
                'show_for_agent' => true ,
                'show_only_whene_active' => false ,
                'show_for_clients' => 'rehagi , lafarge',
                'components' => null
            ] ,
            [   'pid' => 2 ,
                'libelle' => 'elegibilite' ,
                'description' => 'je valide elegibilite',
                'ordre' => 2 ,
                'enabled' => true,
                'active'=> true ,
                'show_for_client' => true,
                'show_for_agent' => true ,
                'show_only_whene_active' => false ,
                'show_for_clients' => 'rehagi , lafarge',
                'components' => null
            ] ,
            [   'pid' => 2 ,
                'libelle' => 'visite technique' ,
                'description' => 'je confirme ma visite technique',
                'ordre' => 3 ,
                'enabled' => true,
                'active'=> false ,
                'show_for_client' => true,
                'show_for_agent' => true ,
                'show_only_whene_active' => false ,
                'show_for_clients' => 'rehagi , lafarge',
                'components' => "importStatus"
            ] ,
            [   'pid' => 3 ,
                'libelle' => 'visite technique 2' ,
                'description' => 'je confirme ma visite technique',
                'ordre' => 1 ,
                'enabled' => true,
                'active'=> false ,
                'show_for_client' => true,
                'show_for_agent' => true ,
                'show_only_whene_active' => false ,
                'show_for_clients' => 'rehagi , lafarge',
                'components' => null
            ] ,
            [   'pid' => 3 ,
                'libelle' => 'validation devise' ,
                'description' => 'je valide le devise',
                'ordre' => 2 ,
                'enabled' => true,
                'active'=> false,
                'show_for_client' => true,
                'show_for_agent' => true ,
                'show_only_whene_active' => false ,
                'show_for_clients' => 'rehagi , lafarge',
                'components' => null
            ] ,
            [   'pid' => 3 ,
                'libelle' => 'imporation' ,
                'description' => 'importer les fichier x',
                'ordre' => 3 ,
                'enabled' => true,
                'active'=> true ,
                'show_for_client' => true,
                'show_for_agent' => true ,
                'show_only_whene_active' => false ,
                'show_for_clients' => 'rehagi , lafarge',
                'components' => "importStatus"
            ] ,
        ];

        foreach ($status as $statu) {
            $st = new \App\Models\Status();
            $st->pid = $statu['pid'];
            $st->libelle = $statu['libelle'];
            $st->description = $statu['description'];
            $st->ordre = $statu['ordre'];
            $st->enabled = $statu['enabled'];
            $st->active = $statu['active'];
            $st->show_for_client = $statu['show_for_client'];
            $st->show_for_agent = $statu['show_for_agent'];
            $st->show_only_when_active = $statu['show_only_whene_active'];
            $st->show_for_clients = $statu['show_for_clients'] ;
            $st->components = $statu['components'] ;
            $st->save();
        }
    }
}
