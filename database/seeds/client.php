<?php

use Illuminate\Database\Seeder;

class client extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = ['lafarge' , 'rehagi' , 'test'];
        foreach ($names as $name) {
            $client = new \App\Models\Client();
            $client->name = $name ;
            $client->save();
        }

    }
}
