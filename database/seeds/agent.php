<?php

use Illuminate\Database\Seeder;

class agent extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = ['agent 1 ' , 'agent 2' , 'test'];
        foreach ($names as $name) {
            $client = new \App\Models\Agent();
            $client->name = $name ;
            $client->save();
        }
    }
}
